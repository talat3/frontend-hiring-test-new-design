import Calls from './Calls'

const CallPage = ({CallsRecord}) => {
  return (
    <Calls CallsRecord={CallsRecord}/>
  )
}

export default CallPage