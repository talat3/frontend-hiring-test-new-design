import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import helper, { messageType } from "../Helper/helper";
import {
  Button,
  Container,
  Dropdown,
  Menu,
  PageHeader,
  Table,
  Text,
} from "../../libs/styled-components";
import {
  MainHeading,
  StyledButton,
  StyledContainer,
  StyledImage,
  StyledPagination,
} from "./element";
import { ColumnsType } from "antd/es/table";
import {
  CallPageProps,
  DataType,
  pagestate,
  CallsDetail,
  ModalData,
} from "../../libs/types/pages";
import { setCookie, getCookie, deleteCookie, hasCookie } from "cookies-next";
import { Space } from "antd";
import { DownOutlined } from "@ant-design/icons";
import CallDetail from "../CallDetail/CallDetail";
import { defaultTheme } from "../../libs/theme-style/defaultTheme";
import LogOut from "../Logout/Logout";

const Calls: React.FC<CallPageProps> = ({ CallsRecord }) => {
  const route = useRouter();
  const [data, setdata] = useState<CallsDetail[]>([]);
  const [alldata, setalldata] = useState<CallsDetail[]>([]);
  const [pages, setpages] = useState<pagestate>({
    currentpage: 1,
    totalpages: 1,
    countPerPage: 10,
    offset: 0,
  });
  const [modalData, setModalData] = useState<ModalData>({
    visible: false,
    data: "",
    index: "",
  });
  const [selectedFilter, setSelectedFilter] = useState<string>("Status");
  const [LogoutModalVisible, setLogoutModalVisible] = useState<boolean>(false);
  const [tableLoading, setTableLoading] = useState<boolean>(false);

  useEffect(() => {
    if (CallsRecord?.statusCode == 401 || helper.isEmptyString(CallsRecord)) {
      route.push("/");
    } else {
      sortAscending(CallsRecord?.nodes);
      setalldata(CallsRecord?.nodes);
      setpages({
        currentpage: 1,
        totalpages: CallsRecord?.totalCount,
        countPerPage: 10,
        offset: 1,
      });
      refreshToken();
    }

    setInterval(() => {
      refreshToken();
    }, 480000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getCallData = (e: number) => {
    if (hasCookie("UserToken")) {
      setTableLoading(true);
      let gettoken: any = getCookie("UserToken");
      gettoken = JSON.parse(gettoken).token;
      axios
        .get(
          `https://frontend-test-api.aircall.io/calls?offset=${
            (e - 1) * 10
          }&limit=${pages.countPerPage}`,
          {
            headers: {
              Authorization: `bearer ${gettoken}`,
            },
          }
        )
        .then(async (res) => {
          if (res.status == 200) {
            setalldata(res.data.nodes);
            sortAscending(res.data.nodes);
            setpages({
              currentpage: e,
              totalpages: res.data.totalCount,
              countPerPage: 10,
              offset: (e - 1) * 10,
            });
            setSelectedFilter("Status");
            setTableLoading(false);
          } else {
            setTableLoading(false);
            helper.toastNotification(
              "Unable to process request.",
              messageType.failed
            );
          }
        })
        .catch((error) => {
          setTableLoading(false);
          helper.toastNotification("Login Again.", messageType.failed);
          deleteCookie("UserToken");
          route.push("/");
          console.log(error, "error");
        });
    } else {
      route.push("/");
    }
  };

  const refreshToken = () => {
    if (hasCookie("UserToken")) {
      let gettoken: any = getCookie("UserToken");
      gettoken = JSON.parse(gettoken).token;
      axios
        .post(
          `https://frontend-test-api.aircall.io/auth/refresh-token`,
          {},
          {
            headers: {
              Authorization: `bearer ${gettoken}`,
            },
          }
        )
        .then(async (res) => {
          if (res.status == 201) {
            let authToken = { token: res.data.access_token };
            setCookie("UserToken", JSON.stringify(authToken));
            // helper.toastNotification(
            //   "Token Updated Successfully.",
            //   messageType.success
            // );
          }
        })
        .catch((error) => {
          deleteCookie("UserToken");
          route.push("/");
          console.log(error, "error");
        });
    } else {
      route.push("/");
    }
  };

  const sortAscending = (data: any) => {
    setdata(
      data.sort((a: any, b: any) => (a.created_at > b.created_at ? -1 : 1))
    );
  };

  const archived = (e: boolean, index: number, item: CallsDetail) => {
    if (hasCookie("UserToken")) {
      let gettoken: any = getCookie("UserToken");
      gettoken = JSON.parse(gettoken).token;
      axios
        .put(
          `https://frontend-test-api.aircall.io/calls/${item.id}/archive`,
          { item },
          {
            headers: {
              Authorization: `bearer ${gettoken}`,
            },
          }
        )
        .then(async (res) => {
          if (res.status == 200) {
            let array: CallsDetail[] = [...data];
            array[index].is_archived = !array[index].is_archived;
            setdata(array);
            helper.toastNotification(
              "Updated Successfully.",
              messageType.success
            );
          } else {
            helper.toastNotification(
              "Call Does not Exist.",
              messageType.failed
            );
          }
        })
        .catch((error) => {
          helper.toastNotification("Call Does not Exist.", messageType.failed);
          console.log(error, "error");
        });
    } else {
      route.push("/");
    }
  };

  const UpdateNotes = (updatedata: CallsDetail) => {
    setModalData({ ...modalData, data: updatedata });
    let array = [...data];
    array[modalData.index] = updatedata;
    setdata(array);
  };

  const handleCancel = () => {
    setModalData({ visible: false, data: "", index: "" });
  };

  const OpenModal = (index: number, item: CallsDetail) => {
    setModalData({ visible: true, data: item, index: index });
  };

  const CheckStatus = ({ key }) => {
    if (key == "All") {
      setSelectedFilter("All");
      setdata(alldata);
    } else if (key == "Archived") {
      setSelectedFilter("Archived");
      setdata(alldata.filter((call) => call.is_archived == true));
    } else {
      setSelectedFilter("Unarchived");
      setdata(alldata.filter((call) => call.is_archived == false));
    }
  };

  const menu = (
    <Menu
      onClick={(e) => CheckStatus(e)}
      selectable
      defaultSelectedKeys={["All"]}
      selectedKeys={[selectedFilter == "Status" ? "All" : selectedFilter]}
      items={[
        {
          label: "All",
          key: "All",
        },
        {
          label: "Archived",
          key: "Archived",
        },
        {
          label: "Unarchived",
          key: "Unarchived",
        },
      ]}
    />
  );

  const columns: ColumnsType<DataType> = [
    {
      title: "CALL TYPE",
      dataIndex: "call_type",
      render: (text) => (
        <Text
          color={
            text == "missed"
              ? defaultTheme.missedColor
              : text == "answered"
              ? defaultTheme.answeredColor
              : defaultTheme.voicemailColor
          }
        >
          {text.charAt(0).toUpperCase() + text.slice(1)}
        </Text>
      ),
    },
    {
      title: "DIRECTION",
      dataIndex: "direction",
      render: (text) => (
        <Text color={defaultTheme.directionColor}>
          {text.charAt(0).toUpperCase() + text.slice(1)}
        </Text>
      ),
    },
    {
      title: "DURATION",
      dataIndex: "duration",
      render: (text) => (
        <>
          <Text display="block">
            {Math.floor(text / 60)} minutes {text - Math.floor(text / 60) * 60}{" "}
            seconds
          </Text>
          <Text color={defaultTheme.directionColor}>{`(${text} seconds)`}</Text>
        </>
      ),
    },
    {
      title: "FROM",
      dataIndex: "from",
      render: (text) => <Text fontSize="14px">{text}</Text>,
    },
    {
      title: "TO",
      dataIndex: "to",
      render: (text) => <Text fontSize="14px">{text}</Text>,
    },
    {
      title: "VIA",
      dataIndex: "via",
      render: (text) => <Text fontSize="14px">{text}</Text>,
    },
    {
      title: "CREATED AT",
      dataIndex: "created_at",
      render: (text) => (
        <Text fontSize="14px">{helper.formatDateInHashes(text)}</Text>
      ),
    },
    {
      title: "STATUS",
      dataIndex: "is_archived",
      render: (text, item: any, index): any => (
        <Button
          onClick={(e) => archived(e, index, item)}
          size="large"
          backgroundcolor={
            text
              ? defaultTheme.backgroundArchivedColor
              : defaultTheme.backgroundUnarchiveColor
          }
          textcolor={
            text ? defaultTheme.archivedColor : defaultTheme.unarchiveColor
          }
          border="none"
          fontSize="12px"
          width={text ? "64px" : "71px"}
          height="24px"
        >
          {text ? "Archived" : "Unarchive"}
        </Button>
      ),
    },
    {
      title: "ACTIONS",
      dataIndex: "is_archived",
      render: (text, item: any, index): any => (
        <Button
          size="large"
          onClick={(e) => OpenModal(index, item)}
          fontSize="12px"
          width="72px"
          height="24px"
        >
          Add Note
        </Button>
      ),
    },
  ];
  return (
    <Container>
      <PageHeader>
        <StyledImage
          width="50"
          height="50"
          src="/TT Logo.png"
          preview={false}
          alt="logo"
        />
        <StyledButton
          size="large"
          onClick={() => setLogoutModalVisible(true)}
          width="115px"
          height="40px"
        >
          Log out
        </StyledButton>
      </PageHeader>
      <StyledContainer>
        <MainHeading type="h2">Turing Technologies Frontend Test</MainHeading>
        <Text fontSize="14px">Filter by:</Text>
        <Dropdown overlay={menu} trigger={["click"]}>
          <Space>
            {selectedFilter}
            <DownOutlined />
          </Space>
        </Dropdown>
        <Table
          columns={columns}
          dataSource={data}
          pagination={false}
          loading={tableLoading}
        />
        <StyledPagination
          current={pages.currentpage}
          onChange={(e) => getCallData(e)}
          total={pages.totalpages}
          hideOnSinglePage
          size="small"
          showTotal={(total, range) =>
            `${range[0]}-${range[1]} of ${total} items`
          }
        />
      </StyledContainer>
      <CallDetail
        modalData={modalData}
        handleCancel={handleCancel}
        UpdateNotes={UpdateNotes}
      />
      <LogOut
        visible={LogoutModalVisible}
        setLogoutModalVisible={setLogoutModalVisible}
      />
    </Container>
  );
};

export default React.memo(Calls);
