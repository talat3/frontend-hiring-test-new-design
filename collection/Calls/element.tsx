import styled from "styled-components";
import {
  Button,
  Container,
  Image,
  Pagination,
  Selecttag,
} from "../../libs/styled-components";
import {
  HeadingProps,
  PaginationProps,
  Selecttagprops,
} from "../../libs/types/style-components";

export const StyledContainer = styled((props: HeadingProps) => (
  <Container {...props} />
))`
  margin: 0px 40px;
`;

export const MainHeading = styled((props: Selecttagprops) => (
  <Selecttag {...props} />
))`
  margin: 30px 0px;
`;
export const StyledPagination = styled((props: PaginationProps) => (
  <Pagination {...props} />
))`
  .ant-pagination-total-text {
    position: absolute;
    margin-top: 35px;
  }
`;

export const StyledImage = styled((props: any) => <Image {...props} />)`
  &&.ant-image-img {
    width: 50%;
  }
`;

export const StyledButton = styled((props: any) => <Button {...props} />)`
  float: right;
`;
