import styled from "styled-components";
import { Button, Container, Modal, Text } from "../../libs/styled-components";
import {
  Buttonprops,
  HeadingProps,
  ModalProps,
  TextProps,
} from "../../libs/types/style-components";

export const StyledText = styled((props: TextProps) => <Text {...props} />)`
  margin: 10px 0px;
`;

export const StyledTextHeading = styled((props: TextProps) => (
  <Text {...props} />
))`
  width: 72px;
  font-size: 14px;
`;

export const StyledButton = styled((props: Buttonprops) => (
  <Button {...props} />
))`
  margin: 20px 0px;
  width: 488px;
  height: 48px;
`;

export const StyledModal = styled((props: ModalProps) => <Modal {...props} />)`
  .ant-modal-footer {
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

export const StyledContainer = styled((props: HeadingProps) => (
  <Container {...props} />
))`
  min-height: 5px;
  max-height: 70px;
  overflow-y: auto;
`;
