import axios from 'axios';
import { getCookie, hasCookie } from 'cookies-next';
import { useRouter } from 'next/router';
import React, { useState } from 'react'
import { Container, FlexContainer, Text, TextArea } from '../../libs/styled-components';
import { defaultTheme } from '../../libs/theme-style/defaultTheme';
import { CallsDetail, ModalProps, Notes } from '../../libs/types/pages';
import helper, { messageType } from '../Helper/helper';
import { StyledButton, StyledContainer, StyledModal, StyledText, StyledTextHeading } from './element';

const CallDetail:React.FC<ModalProps>= ({modalData,handleCancel,UpdateNotes}) => {
  const router = useRouter();
  const [value,setValue]= useState<string>('')
  const callDetails = modalData.data as CallsDetail

  const submit = () => {
    if (helper.isEmptyString(value)) {
      helper.toastNotification("Input is required.", messageType.failed);
    } else {
      if (hasCookie("UserToken")) {
      let gettoken: any = getCookie("UserToken");
      gettoken = JSON.parse(gettoken).token;
      axios
        .post(
          `https://frontend-test-api.aircall.io/calls/${callDetails.id}/note`,
          {
            content: value,
          },
          {
            headers: {
              Authorization: `bearer ${gettoken}`,
            },
          }
        )
        .then(async (res) => {
          if (res.status == 201) {
            setValue("");
            UpdateNotes(res.data);
            helper.toastNotification(
              "Note added Successfully.",
              messageType.success
            );
          } else {
            helper.toastNotification(
              "Unable to process request.",
              messageType.failed
            );
          }
        })
        .catch((error) => {
          helper.toastNotification(
            "Unable to process request.",
            messageType.failed
          );
          console.log(error, "error");
        });
    }
    else{
      router.push('/')
    }
  }
  };
  
  return (
    <StyledModal visible={modalData.visible}
    title={
    <>
    <StyledText display='block' fontSize='18px'>Add Notes</StyledText>
    <Text color={defaultTheme.modalCallIdColor}>
      {`Call ID ${callDetails.id}`}
      </Text>
    </>
    }
    onCancel={handleCancel}
    afterClose={handleCancel}
    width="540px"
    height="521px"
    footer={[
      <StyledButton key="back" block size="large" onClick={() => submit()}>
        Save
      </StyledButton>
    ]}
    >
      <Container>
        <FlexContainer gap='5px 0px'>
        <StyledTextHeading>Call Type</StyledTextHeading>
        <Text fontSize='14px'
        color={callDetails.call_type == "missed" ? defaultTheme.missedColor
        :callDetails.call_type == "answered" ? defaultTheme.answeredColor:
        defaultTheme.voicemailColor
        }
        >
          {callDetails?.call_type?.charAt(0).toUpperCase() + callDetails?.call_type?.slice(1)}</Text>
        </FlexContainer>
        <FlexContainer gap='5px 0px'>
        <StyledTextHeading>Duration</StyledTextHeading>
        <Text fontSize='14px'>{Math.floor(callDetails.duration / 60)} minutes {callDetails.duration - (Math.floor(callDetails.duration / 60) * 60)} seconds</Text>
        </FlexContainer>
        <FlexContainer gap='5px 0px'>
        <StyledTextHeading>FROM</StyledTextHeading>
        <Text fontSize='14px'>{callDetails.from}</Text>
        </FlexContainer>
        <FlexContainer gap='5px 0px'>
        <StyledTextHeading>TO</StyledTextHeading>
        <Text fontSize='14px'>{callDetails.to}</Text>
        </FlexContainer>
        <FlexContainer gap='5px 0px'>
        <StyledTextHeading>VIA</StyledTextHeading>
        <Text fontSize='14px'>{callDetails.via}</Text>
        </FlexContainer>
        <Text marginTop='10px' fontSize='14px' display='block'>Notes</Text>
        <StyledContainer>
        {
        helper.isArray(callDetails?.notes) && callDetails?.notes.length ?(
                callDetails.notes.map((item: Notes, index: number) => (
                <FlexContainer key={index}>
                  <Text>{index + 1}:</Text>
                  <Text>{item?.content}</Text>
                </FlexContainer>
              ))
          ) : ''}
          </StyledContainer>
        <TextArea 
        value={value}
        onChange={e => setValue(e.target.value)}
        placeholder="Add Notes"
        autoSize={{ minRows: 4, maxRows: 5 }}
        />
      </Container>
    </StyledModal>
  )
}
export default CallDetail;