import styled from "styled-components";
import { Modal } from "../../libs/styled-components";
import { defaultTheme } from "../../libs/theme-style/defaultTheme";

export const StyledModal = styled((props: any) => <Modal {...props} />)`
.ant-btn-primary {
    color:${defaultTheme.btntextColor};
    background-color:${defaultTheme.btnColor};
    border-radius: 4px;
    width:78px;
}
.ant-btn-primary:hover {
    color:#7c70ff;
    background-color:#fff;
}
`;