import { deleteCookie } from 'cookies-next';
import { useRouter } from 'next/router';
import React from 'react'
import { Text } from '../../libs/styled-components';
import { LoginModalProps } from '../../libs/types/pages';
import { StyledModal } from './element';

const LogOut: React.FC<LoginModalProps> = ({visible,setLogoutModalVisible}) => {
    const route = useRouter();
    const handleCancel = () => {
        setLogoutModalVisible(false)
    }

    const Logout = () => {
        deleteCookie("UserToken")
        route.push('/')
      }

  return (
    <StyledModal 
    title="Log Out" 
    visible={visible} 
    onOk={Logout} 
    onCancel={handleCancel}
    afterClose={handleCancel}
    >
        <Text fontSize='14px'>
            Are you sure, You want to log out?
        </Text>
    </StyledModal>
  )
}
export default LogOut;