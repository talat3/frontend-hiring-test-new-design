import styled from "styled-components";
import { HeadingProps } from "../../types/style-components";

export const Heading: React.FC<HeadingProps> = styled((props: HeadingProps) => (
  <div {...props} />
))`
  && {
    text-align: ${({ textalign }) => textalign};
  }
`;
