import styled from "styled-components";
import { Pagination as AntPagination } from "antd";
import { PaginationProps } from "../../types/style-components";
import { defaultTheme } from "../../theme-style/defaultTheme";

export const Pagination = styled((props: PaginationProps) => {
  return <AntPagination {...props} />;
})`
  margin-bottom: 20px;
  padding-bottom: 88px;
  justify-content: center;
  display: flex;
  color: black;
  font-size: 12px;
  font-family: Avenir, sans-serif;
  .ant-pagination-item-active {
    background: ${defaultTheme.btnColor};
    color: ${defaultTheme.btntextColor};
  }
  margin-top: 80px;
  .ant-pagination-options {
    display: none;
  }
  &&.ant-pagination.ant-pagination-mini .ant-pagination-item {
    margin: 0px 10px;
  }
`;
