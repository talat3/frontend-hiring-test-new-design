import styled from "styled-components";
import { Table as AntTable } from 'antd';
import { omitCSSProps, TableProps } from "../../types/style-components";

export const Table = styled((props: TableProps) => {
    const domProps = omitCSSProps(props);
    return <AntTable {...domProps} />;
  })`
  margin-top:20px;
  th.ant-table-cell{
    font-weight: bold;
  }
  .ant-table {
    font-size:12px;
  }
  .ant-table-thead > tr > th {
    color:#232323;
  }
  `