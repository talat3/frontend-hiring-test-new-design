import styled from "styled-components";
import { TextProps } from "../../types/style-components";

export const Text :React.FC<TextProps> = styled((props: TextProps)=> <span {...props}/>)`
color: ${({ color }) => color?color:'#232323'};
font-size: ${({ fontSize }) => fontSize?fontSize:'12px'};
font-weight:medium;
display:${({ display }) => display};
margin-top:${({ marginTop }) => marginTop};
`;