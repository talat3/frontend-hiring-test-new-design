import styled from "styled-components";
import { Button as AntButton } from "antd";
import { Buttonprops } from "../../types/style-components";
import { defaultTheme } from "../../theme-style/defaultTheme";

export const Button: React.FC<Buttonprops> = styled((props: Buttonprops) => {
  return <AntButton {...props} />;
})`
  background-color: ${({ backgroundcolor }) =>
    backgroundcolor ? backgroundcolor : defaultTheme.btnColor};
  color: ${({ textcolor }) =>
    textcolor ? textcolor : defaultTheme.btntextColor};
  border-radius: 4px;
  border: ${({ border }) => border};
  font-size: ${({ fontSize }) => fontSize};
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${({ width }) => width};
  height: ${({ height }) => height};
`;
