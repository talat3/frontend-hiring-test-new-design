import styled from "styled-components";
import { Modal as AntModal } from "antd";
import { ModalProps } from "../../types/style-components";
import { defaultTheme } from "../../theme-style/defaultTheme";

export const Modal: React.FC<ModalProps> = styled((props: ModalProps) => {
  return <AntModal {...props} />;
})`
.anticon{
  color:${defaultTheme.modalCallIdColor};
  font-size: 17px;
}
`;
