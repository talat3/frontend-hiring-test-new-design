import styled from "styled-components";



export const FlexContainer = styled((props) => (
    <div {...props} />
  ))`
  display: flex;
  gap: ${({gap})=>gap};
  padding:5px 0px;
  `;