import styled from "styled-components";
import { Form as AntForm} from "antd";
import { FormItemProps, FormProps, omitCSSProps } from "../../types/style-components";

export const Form: React.FC<FormProps> = styled((props: FormProps) => {
    const domProps = omitCSSProps(props);
  return <AntForm {...domProps} />;
})``;

export const FormItem: React.FC<FormItemProps> = styled((props: FormItemProps) => {
    const domProps = omitCSSProps(props);
  return <AntForm.Item {...domProps} />;
})``;