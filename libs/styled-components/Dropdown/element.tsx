import { Dropdown as AntDropdown } from "antd";
import styled from "styled-components";
import { defaultTheme } from "../../theme-style/defaultTheme";
import { DropdownProps } from "../../types/style-components";

export const Dropdown = styled((props: DropdownProps) => {
  return <AntDropdown {...props} />;
})`
  color: ${defaultTheme.statusColor};
  font-size: 14px;
  font-weight: medium;
  margin-left: 10px;
  cursor: pointer;
  .anticon {
    font-size: 12px;
    margin-bottom: 7px;
    margin-left: 4px;
    color: #575962;
  }
`;
