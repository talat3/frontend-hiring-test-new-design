import { Switch as AntSwitch } from "antd";
import styled from "styled-components";
import { omitCSSProps, SwitchProps } from "../../types/style-components";

export const Switch = styled((props: SwitchProps) => {
  const domProps = omitCSSProps(props);
  return <AntSwitch {...domProps} />;
})`
`;
