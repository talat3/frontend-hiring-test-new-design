import { Menu as AntMenu} from 'antd';
import styled from 'styled-components';
import { defaultTheme } from '../../theme-style/defaultTheme';
import { MenuProps, omitCSSProps } from '../../types/style-components';

export const Menu = styled((props: MenuProps) => {
  const restProps = omitCSSProps(props);
  return <AntMenu {...restProps} />;
})`
min-width: 200px;
.ant-dropdown-menu-item-selected{
    color:${defaultTheme.menuColor};
    background-color:${defaultTheme.backMenuColor};
    border-radius: 4px;
}
.ant-dropdown-menu-item{
  margin: 0px 10px;
}
`;