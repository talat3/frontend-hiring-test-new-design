import { Tooltip as AntTooltip } from 'antd';
import styled from 'styled-components';
import { TooltipProps } from '../../types/style-components';

export const Tooltip = styled((props: TooltipProps) => {
  return <AntTooltip {...props} />;
})``;
