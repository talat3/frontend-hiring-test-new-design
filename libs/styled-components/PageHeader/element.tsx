import { PageHeader as AntPageHeader} from 'antd';
import styled from 'styled-components';
import { PageHeaderProps } from '../../types/style-components';

export const PageHeader = styled((props: PageHeaderProps) => {
  return <AntPageHeader {...props} />;
})`
    padding: 20px;
    margin:10px;
`;
