import styled from "styled-components";
import { Input as AntInput} from "antd";
import { omitCSSProps } from "../../types/style-components";
import { InputPasswordProps, InputProps } from "../../types/style-components/inputprops";
import { defaultTheme } from "../../theme-style/defaultTheme";

export const { TextArea } = AntInput;

export const Input: React.FC<InputProps> = styled((props: InputProps) => {
    const domProps = omitCSSProps(props);
  return <AntInput {...domProps} />;
})`
border-color:${defaultTheme.borderColor};
`;

export const PasswordInput: React.FC<InputPasswordProps> = styled((props: InputPasswordProps) => {
    const domProps = omitCSSProps(props);
  return <AntInput.Password {...domProps} />;
})`
  border-color:${defaultTheme.borderColor};
`;