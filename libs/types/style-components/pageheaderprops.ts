import { PageHeaderProps as AntPageHeaderProps } from 'antd/es/page-header';

import { BoxProperties } from './css';

export type PageHeaderProps = AntPageHeaderProps & BoxProperties;
