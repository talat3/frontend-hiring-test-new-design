import { DropDownProps as AntDropdownProps } from 'antd/es/dropdown';

import { BoxProperties } from './css';

export type DropdownProps = AntDropdownProps & BoxProperties;
