import {
    MenuItemProps as AntMenuItemProps,
    MenuProps as AntMenuProps,
  } from 'antd/es/menu';
  
  import { BoxProperties } from './css';
  
  export type MenuProps = AntMenuProps & BoxProperties;
  export type MenuItemProps = AntMenuItemProps & BoxProperties;
  