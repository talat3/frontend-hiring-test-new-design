import React from "react";

export interface CallPageProps {
  CallsRecord: {
    hasNextPage: boolean;
    nodes: CallsDetail[];
    totalCount: number;
    statusCode?: number;
  };
}

export type pagestate = {
  currentpage: number;
  totalpages: number;
  countPerPage: number;
  offset: number;
};

export interface CallsDetailProps {
  CallsRecord: CallsDetail;
  id: string;
}

export interface LoginCredentials {
  username: string;
  password: any;
}

export interface LoginPageProps {
  redirect?: boolean;
}

export interface DataType {
  key: React.Key;
  call_type: string;
  index: number;
  from: string;
  direction: string;
  date: string;
  action: any;
}

export interface CallsDetail {
  call_type: string;
  created_at: string;
  direction: string;
  duration: number;
  from: string;
  id: string;
  is_archived: boolean;
  notes: Notes[];
  to: string;
  via: string;
}

export interface Notes {
  id: string;
  content: string;
}

export interface ModalData{
  visible:boolean,
  data?:CallsDetail | string,
  index?:number | string,
}

export interface ModalProps{
  modalData:ModalData,
  handleCancel:()=>void,
  UpdateNotes:(updatedata:CallsDetail)=>void,
}

export interface LoginModalProps{
  visible:boolean,
  setLogoutModalVisible:React.Dispatch<React.SetStateAction<boolean>>,
}