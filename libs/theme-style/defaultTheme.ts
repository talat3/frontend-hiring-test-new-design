import { Theme } from "../types/style-components";

export const defaultTheme:Theme ={
    backgroundColor:"#000",
    color:"#fff",
    borderColor:"#4F46F8",
    background:"#000",
    btntextColor:"#FFFFFF",
    btnColor:"#4F46F8",
    textColor:"#000",
    voicemailColor:"#5777EB",
    missedColor:"#C91D3E",
    answeredColor:"#1DC9B7",
    statusColor:"#4F46F8",
    archivedColor:"#1DC9B7",
    backgroundArchivedColor:"#EDFBFA",
    unarchiveColor:"#AAAAAA",
    backgroundUnarchiveColor:"#EEEEEE",
    modalCallIdColor:"#4F46F8",
    menuColor:"#4F46F7",
    backMenuColor:"#D8D6FD",
    directionColor:"#325AE7"
}