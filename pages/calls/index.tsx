import { hasCookie } from "cookies-next";
import { GetServerSideProps, NextPage } from "next";
import React from "react";
import Calls from "../../collection/Calls/Calls";
import { CallApi } from "../../libs/api";
import { CallPageProps } from "../../libs/types/pages";

const CallPage: NextPage<CallPageProps> = ({ CallsRecord }) => {
  return <Calls CallsRecord={CallsRecord} />;
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  if (hasCookie("UserToken", context)) {
    const isLogedin: any = await CallApi(context);
    if (isLogedin == false) {
      return {
        redirect: {
          permanent: false,
          destination: `/`,
        },
      };
    } else {
      const CallsRecord = isLogedin;
      return { props: { CallsRecord } };
    }
  } else {
    return {
      redirect: {
        permanent: false,
        destination: `/`,
      },
    };
  }
};

export default React.memo(CallPage);
