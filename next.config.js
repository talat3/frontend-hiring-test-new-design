/** @type {import('next').NextConfig} */
const withLess = require('next-with-less');
const withPlugins = require('next-compose-plugins');
const path = require("path");

const pathToLessFileWithVariables = path.resolve(
  "./libs/theme-style/varible.less"
);

const plugins = [
  withLess({
    lessLoaderOptions: {
      /* ... */
      additionalData: (content) =>
        `${content}\n\n@import '${pathToLessFileWithVariables}';`,
    },
  })
];

const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  optimizeFonts: false,
}

module.exports = withPlugins([...plugins], nextConfig);
